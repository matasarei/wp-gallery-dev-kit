<?php

$galleryId = empty($gallery) ? '' : $gallery['id'];

$galleryName = empty($gallery) ?
    filter_input(INPUT_GET, 'wp_gdk_gallery_name') :
    $gallery['name']
;

$galleryDescription = empty($gallery) ?
    filter_input(INPUT_GET, 'wp_gdk_gallery_description') :
    $gallery['description']
;
?>

<h1 class="wp-heading-inline"><?php echo $gallery['name']; ?></h1>

<form method="post">
    <div id="universal-message-container">
        <h2><?php _e('Edit gallery', 'wp-gdk') ?></h2>

        <p>
            <label><?php _e('Gallery name', 'wp-gdk') ?></label>
            <br>
            <input type="text" name="wp_gdk_gallery_name"
                   placeholder="<?php _e('Gallery name', 'wp-gdk') ?>" size="64" value="<?php echo $galleryName ?>">
        </p>
        <p>
            <label><?php _e('Gallery description', 'wp_gdk') ?></label>
            <br>
            <textarea name="wp_gdk_gallery_description" placeholder="<?php _e('Gallery description', 'wp-gdk') ?>"
                      cols="64" rows="8"><?php echo $galleryDescription; ?></textarea>
        </p>

        <?php wp_nonce_field(); ?>
        <?php submit_button(); ?>
</form>
