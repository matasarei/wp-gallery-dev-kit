<?php

use wp_gallery_dev_kit\WP_GDK_Repository;

$gallery_id = empty($gallery) ? '' : $gallery['id'];

if (!isset($items)) {
    $items = null;
}

if (empty($items)) {
    wp_gdk_print_error(__('No media currently added to this gallery, please add one.', 'wp-gdk'));
}

$sortUrl = admin_url('admin.php?' . http_build_query([
    'page' => 'wp-gdk-admin-page',
    'action' => 'update_order',
    'gallery_id' => $gallery_id
]));

?>

<p>
    <a class="button-primary" href="<?php echo admin_url('admin.php?' . http_build_query([
            'page' => 'wp-gdk-admin-page',
        ])) ?>"><?php _e('All galleries', 'wp-gdk'); ?>
    </a>
</p>

<h1 class="wp-heading-inline"><?php echo $gallery['name']; ?></h1>
<a class="page-title-action" href="<?php echo admin_url('admin.php?' . http_build_query([
        'page' => 'wp-gdk-admin-page',
        'action' => 'edit_gallery',
        'gallery_id' => $gallery_id
    ])) ?>"><?php _e('Edit gallery', 'wp-gdk'); ?>
</a>

<form method="POST" action="<?php echo wp_gdk_get_edit_media_url($gallery_id) ?>">
    <h2><?php _e('Add new media', 'wp-gdk'); ?></h2>
    <p>
        <label for="wp_gdk_insert_single_media" ><?php _e('Single', 'wp-gdk'); ?></label>
        <input type="radio" value="single" id="wp_gdk_insert_single_media" name="wp_gdk_insert_mode" checked="checked">
        <label for="wp_gdk_insert_multiple_media"><?php _e('Multiple', 'wp-gdk'); ?></label>
        <input type="radio" value="multiple" id="wp_gdk_insert_multiple_media" name="wp_gdk_insert_mode">
    </p>
    <div id="wp_gdk_single_media_form">
        <p>
            <label for="wp_gdk_single_media_input">
                <span style="color:red">
                    <b>*</b>
                </span>
                <?php _e('Media URL (a photo or resource preview)', 'wp-gdk') ?>
            </label>
            <br>
            <input type="text" id="wp_gdk_single_media_input" name="wp_gdk_single_media_url" size="64">
            <input id="wp_gdk_select_single_media_btn" type="button" class="button-primary" value="<?php _e('Select from Media Library', 'wp-gdk'); ?>">
        </p>
        <p>
            <label for="wp_gdk_resource_input"><?php _e('Resource URL (a file, YouTube or Vimeo video), optional', 'wp-gdk') ?></label>
            <br>
            <input type="text" id="wp_gdk_resource_input" name="wp_gdk_resource_input" size="64">
        </p>
        <p>
            <label for="wp_gdk_title_input"><?php _e('Short description, optional', 'wp-gdk') ?></label>
            <br>
            <input type="text" id="wp_gdk_title_input" name="wp_gdk_title_input" size="64">
        </p>
    </div>
    <div id="wp_gdk_multiple_media_form" style="display:none">
        <input id="wp_gdk_select_multiple_media_btn" type="button" class="button-primary" value="<?php _e('Select from Media Library', 'wp-gdk'); ?>">
        (<?php _e('hold Ctrl to select few items', 'wp-gdk') ?>)
        <p>
            <label for="wp_gdk_multiple_media_input">
                <span style="color:red">
                    <b>*</b>
                </span>
                <?php _e('Images URLs', 'wp-gdk') ?>
            </label>
            <br>
            <textarea id="wp_gdk_multiple_media_input" name="wp_gdk_multiple_media_input" cols="128" rows="16"></textarea>
        </p>
    </div>
    <?php wp_nonce_field(); ?>
    <?php submit_button(__('Insert Media', 'wp-gdk')); ?>
</form>

<?php if (!empty($items)): ?>
    <h2>Gallery</h2>
    <table class="wp-gdk-gallery-items wp-list-table widefat fixed striped items"
           data-galleryid="<?php echo $gallery_id ;?>" data-sorturl="<?php echo $sortUrl; ?>">
        <thead>
            <tr>
                <th width="5%"></th>
                <th width="25%"><?php _e('Image') ?></th>
                <th width="25%"><?php _e('Description') ?></th>
                <th width="25%"><?php _e('Resource', 'wp-gdk') ?></th>
                <th width="25%"></th>
            </tr>
        </thead>
        <tbody class="the-list">
        <?php foreach($items as $item): ?>
            <tr class="wp-gdk-gallery-item" data-mediaid="<?php echo $item['id'] ?>">
                <td class="wp-gdk-drag-and-drop">&varr;</td>
                <td class="wp-gdk-item-image">
                    <a href="<?php echo $item['url'] ?>" target="_blank">
                        <img src="<?php echo $item['url'] ?>">
                    </a>
                </td>
                <td><?php echo $item['title'] ?></td>
                <td>
                    <?php if(!empty($item['link'])): ?>
                        <a href="<?php echo $item['link'] ?>" target="_blank"><?php echo $item['link'] ?></a>
                    <?php endif; ?>
                </td>
                <td class="wp-gdk-list-actions">
                    <a data-action="remove" class="button-secondary" href="<?php echo admin_url('admin.php?' . http_build_query([
                            'page' => 'wp-gdk-admin-page',
                            'action' => 'remove_item',
                            'gallery_id' => $gallery_id,
                            'media_id' => $item['id'],
                        ])) ?>">&#10060; <?php _e('Remove'); ?>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>
