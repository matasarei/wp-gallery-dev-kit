<?php

namespace wp_gallery_dev_kit;

class WP_GDK_Repository
{
    const TABLE_GALLERIES = 'gdk_galleries';
    const TABLE_MEDIA = 'gdk_media';

    const RESOURCE_YOUTUBE = 'youtube';
    const RESOURCE_VIMEO = 'vimeo';
    const RESOURCE_OTHER = 'other';

    /**
     * @var \wpdb;
     */
    protected $wpdb;

    /**
     * @param \wpdb $wpdb
     */
    public function __construct(\wpdb $wpdb)
    {
        $this->wpdb = $wpdb;
    }

    /**
     * @param string $name
     * @param string $description
     *
     * @return int
     */
    public function addGallery($name, $description)
    {
        $this->wpdb->insert($this->wpdb->prefix . self::TABLE_GALLERIES, [
            'name' => trim(strip_tags($name)),
            'description' => trim(strip_tags($description)),
        ]);

        return $this->wpdb->insert_id;
    }

    /**
     * @param int $id
     * @param string $name
     * @param string $description
     */
    public function updateGallery($id, $name, $description)
    {
        $this->wpdb->update(
            $this->wpdb->prefix . self::TABLE_GALLERIES,
            [
                'name' => $name,
                'description' => $description
            ],
            [
                'id' => $id
            ]
        );
    }

    /**
     * @param int $id
     *
     * @return array|null
     */
    public function getGallery($id)
    {
        return $this->wpdb->get_row(
            sprintf(
                'SELECT * FROM %s WHERE id = "%d"',
                $this->wpdb->prefix . self::TABLE_GALLERIES,
                $id
            ),
            ARRAY_A
        );
    }

    /**
     * @param string $like
     * @param string $order_by
     * @param string $order
     *
     * @return array
     */
    public function getAllGalleries($like = '', $order_by = 'id', $order = 'desc')
    {
        return $this->wpdb->get_results(
            sprintf(
                'SELECT * FROM %s WHERE %s %s',
                $this->wpdb->prefix . self::TABLE_GALLERIES,
                $this->wpdb->prepare('name LIKE %s', '%' . $this->wpdb->esc_like($like) . '%'),
                sanitize_sql_orderby(sprintf('ORDER BY `%s` %s', $order_by, $order))
            ),
            ARRAY_A
        );
    }

    /**
     * @param int $galleryId
     * @param string $url
     * @param string|null $title
     * @param string|null $link
     *
     * @return int
     */
    public function addMedia($galleryId, $url, $title = null, $link = null)
    {
        $this->wpdb->insert($this->wpdb->prefix . self::TABLE_MEDIA, [
            'gallery_id' => (int)$galleryId,
            'url' => trim($url),
            'link' => null === $title ? null : trim($title),
            'title' => null === $link ? null : trim($link)
        ]);

        return $this->wpdb->insert_id;
    }

    /**
     * @param int $mediaId
     * @param string $url
     * @param string|null $title
     * @param string|null $link
     */
    public function updateMedia($mediaId, $url, $title = null, $link = null)
    {
        $this->wpdb->update(
            $this->wpdb->prefix . self::TABLE_MEDIA,
            [
                'url' => trim($url),
                'link' => null === $title ? null : trim($title),
                'title' => null === $link ? null : trim($link)
            ],
            [
                'id' => $mediaId
            ]
        );
    }

    /**
     * @param int $mediaId
     */
    public function removeMedia($mediaId)
    {
        $this->wpdb->delete($this->wpdb->prefix . self::TABLE_MEDIA, [
            'id' => $mediaId
        ]);
    }

    /**
     * @param int $id
     */
    public function removeGallery($id)
    {
        $this->wpdb->delete($this->wpdb->prefix . self::TABLE_MEDIA, [
            'gallery_id' => $id
        ]);

        $this->wpdb->delete($this->wpdb->prefix . self::TABLE_GALLERIES, [
            'id' => $id
        ]);
    }

    /**
     * @param $galleryId
     *
     * @return array
     */
    public function getAllMedia($galleryId)
    {
        $records = $this->wpdb->get_results(
            sprintf(
                'SELECT * FROM %s WHERE gallery_id = "%d" ORDER BY sort',
                $this->wpdb->prefix . self::TABLE_MEDIA,
                $galleryId
            ),
            ARRAY_A
        );

        $items = [];

        foreach ($records as $record) {
            $items[] = $this->parseMedia($record);
        }

        return $items;
    }

    /**
     * @param int $id
     *
     * @return array|null
     */
    public function getMedia($id)
    {
        $media = $this->wpdb->get_row(
            sprintf(
                'SELECT * FROM %s WHERE id = "%d"',
                $this->wpdb->prefix . self::TABLE_MEDIA,
                $id
            ),
            ARRAY_A
        );

        if (empty($media)) {
            return null;
        }

        return $this->parseMedia($media);
    }

    /**
     * @param $galleryId
     * @param $mediaIds
     */
    public function updateOrder($galleryId, $mediaIds)
    {
        $this->wpdb->update(
            $this->wpdb->prefix . self::TABLE_MEDIA,
            [
                'sort' => 0
            ],
            [
                'gallery_id' => $galleryId
            ]
        );

        foreach ($mediaIds as $order => $mediaId) {
            $this->wpdb->update(
                $this->wpdb->prefix . self::TABLE_MEDIA,
                [
                    'sort' => $order
                ],
                [
                    'id' => $mediaId
                ]
            );
        }
    }

    /**
     * @param array $media
     *
     * @return array
     */
    protected function parseMedia($media)
    {
        $type = self::RESOURCE_OTHER;

        if (preg_match("/youtu\.?be/", $media['link'])) {
            $type = self::RESOURCE_YOUTUBE;
        } elseif (preg_match('/vimeo/', $media['link'])) {
            $type = self::RESOURCE_VIMEO;
        }

        return [
            'id' => $media['id'],
            'url' => $media['url'],
            'link' => $media['link'],
            'link_type' => $type,
            'title' => $media['title'],
        ];
    }

    /**
     * @return self
     */
    static public function getInstance()
    {
        global $wpdb;

        static $instance;

        if (empty($instance)) {
            $instance = new self($wpdb);
        }

        return $instance;
    }
}
