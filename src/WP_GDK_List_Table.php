<?php

namespace wp_gallery_dev_kit;

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class WP_GDK_List_Table extends \WP_List_Table
{
    public function __construct()
    {
        parent::__construct([
            'ajax'     => false
        ]);
    }

    /**
     * @return string
     */
    protected function get_primary_column_name()
    {
        return 'id';
    }

    /**
     * @return array
     */
    public function get_columns()
    {
        return [
            'id'=>__('ID'),
            'name'=>__('Name'),
            'description'=>__('Description'),
            'actions' => ''
        ];
    }

    /**
     * @return array
     */
    public function get_sortable_columns()
    {
        return [
            'id' => ['id', true],
            'name' => ['name', true]
        ];
    }

    public function prepare_items()
    {
        $this->_column_headers = [
            $this->get_columns(),
            [], // hidden columns
            $this->get_sortable_columns()
        ];

        $order = filter_input(INPUT_GET, 'order');
        $order_by = filter_input(INPUT_GET, 'orderby');

        $this->items = WP_GDK_Repository::getInstance()->getAllGalleries(
            '',
            empty($order_by) ? 'id' : $order_by,
            empty($order) ? 'desc' : $order
        );

        $this->set_pagination_args([
            'total_items' => count($this->items),
            'total_pages' => 1,
            'per_page' => count($this->items)
        ]);
    }

    public function display_rows()
    {
        $columns = array_keys($this->get_columns());

        $records = $this->items;

        if (empty($records)) {
            return;
        }

        foreach ($records as $record) {
            $edit_media_url = wp_gdk_get_edit_media_url($record['id']);

            echo sprintf('<tr id="%s">', $record['id']);

            foreach ($columns as $column) {
                if (in_array($column, ['id', 'name'], true)) {
                    echo sprintf('<td><a href="%s"><b>%s</b></td>', $edit_media_url, strip_tags($record[$column]));

                    continue;
                }

                if ('actions' === $column) {
                    echo
                        '<td class="wp-gdk-list-actions">' .
                        sprintf(
                            '<button class="button-secondary" data-clipboard-text="%d"><b>' .  __('Copy ID', 'wp-gdk') . '</b></button>&nbsp;',
                            $record['id']
                        ) .
                        sprintf(
                            '<a href="%s" class="button-primary"><b>' .  __('Edit Media', 'wp-gdk') . '</b></a>&nbsp;',
                            $edit_media_url
                        ) .
                        sprintf(
                            '<a href="%s" class="button-secondary">' . __('Edit Gallery', 'wp-gdk') . '</a>&nbsp;',
                            admin_url('admin.php?' . http_build_query([
                                'page' => 'wp-gdk-admin-page',
                                'action' => 'edit_gallery',
                                'gallery_id' => $record['id']
                            ]))
                        ) .
                        sprintf(
                            '<a href="%s" class="button-secondary" data-action="remove">&#10060; ' .  __('Remove') . '</a>&nbsp;',
                            admin_url('admin.php?' . http_build_query([
                                'page' => 'wp-gdk-admin-page',
                                'action' => 'remove_gallery',
                                'gallery_id' => $record['id']
                            ]))
                        ) .
                        '</td>'
                    ;

                    continue;
                }

                echo sprintf('<td>%s</td>', strip_tags($record[$column]));
            }

            echo '</tr>';
        }
    }
}
