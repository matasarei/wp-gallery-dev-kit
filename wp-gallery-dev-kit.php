<?php
/**
 * Plugin Name: WP Gallery Development Kit
 * Plugin URI: https://github.com/matasarei/wp-gallery-dev-kit
 * Description: A development kit for gallery plugins or themes development
 * Version: 1.0.0
 * Author: Yevhen Matasar
 * Author URI: http://hcnotes.in.ua/
 * Text Domain: wp-gdk
 * Licence: GPL2
 */

require_once plugin_dir_path(__FILE__) . 'src/WP_GDK_List_Table.php';
require_once plugin_dir_path(__FILE__) . 'src/WP_GDK_Repository.php';

use wp_gallery_dev_kit\WP_GDK_List_Table;
use wp_gallery_dev_kit\WP_GDK_Repository;

register_activation_hook(__FILE__,'wp_gdk_activation');

add_action('admin_menu', 'register_wp_gdk_admin_page');
add_action('admin_enqueue_scripts', 'register_wp_gdk_admin_resources');

/**
 * Register admin page resources, like scripts and styles
 */
function register_wp_gdk_admin_resources()
{
    wp_enqueue_media();

    wp_enqueue_script('masonry', '//cdnjs.cloudflare.com/ajax/libs/jquery-sortable/0.9.13/jquery-sortable-min.js');
    wp_enqueue_script('clipboard', '//cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js');

    wp_register_script(
        'wp-gdk-js',
        plugins_url('script.js' , __FILE__ ),
        [
            'jquery'
        ]
    );
    wp_enqueue_script('wp-gdk-js');

    wp_register_style('wp-gdk-css', plugins_url('style.css',__FILE__ ));
    wp_enqueue_style('wp-gdk-css');

    load_plugin_textdomain('wp-gdk', false, dirname(plugin_basename(__FILE__)));
}

/**
 * Register admin page in the menu
 *
 * @param string $page_title
 * @param string $menu_title
 */
function register_wp_gdk_admin_page($page_title = 'WordPress Gallery Dev Kit', $menu_title = 'Gallery Dev Kit')
{
    remove_menu_page('wp-gdk-admin-page');

    add_menu_page(
        $page_title,
        $menu_title,
        'administrator',
        'wp-gdk-admin-page',
        'wp_gdk_admin_page',
        'dashicons-images-alt'
    );
}

/**
 * Plugin admin page handler (all actions and layout)
 */
function wp_gdk_admin_page()
{
    $isPost = 'POST' === filter_input(INPUT_SERVER, 'REQUEST_METHOD');
    $id = (int)filter_input(INPUT_GET, 'gallery_id');

    echo '<div class="wrap wp-gdk-admin-page">';

    switch (filter_input(INPUT_GET, 'action')) {
        case "add_gallery":
            if ($isPost) {
                wp_gdk_admin_save_gallery();
            } else {
                wp_gdk_admin_edit_gallery();
            }

            break;
        case "edit_gallery":
            if ($isPost) {
                wp_gdk_admin_save_gallery();

                return;
            }

            if (empty($id)) {
                wp_gdk_print_error(__('Gallery ID must be passed via `gallery_id` url parameter!', 'wp-dkg'));

                return;
            }

            wp_gdk_admin_edit_gallery($id);

            break;
        case "edit_items":
            if (empty($id)) {
                wp_gdk_print_error(__('Gallery ID must be passed via `gallery_id` url parameter!', 'wp-dgk'));

                return;
            }

            if ($isPost) {
                wp_gdk_add_medias($id);
            }

            wp_gdk_all_media($id);

            break;
        case "update_order":
            wp_gdk_admin_update_order($id);

            return;
        case "remove_item":
            $mediaId = (int)filter_input(INPUT_GET, 'media_id');

            if (empty($id) || empty($mediaId)) {
                wp_gdk_print_error(__('Media ID must be passed via `media_id` url parameter!', 'wp-gdk'));

                return;
            }

            WP_GDK_Repository::getInstance()->removeMedia($mediaId);

            wp_gdk_all_media($id);

            break;
        case "remove_gallery":
            if (empty($id)) {
                wp_gdk_print_error(__('Gallery ID must be passed via `gallery_id` url parameter!', 'wp-gdk'));

                return;
            }

            WP_GDK_Repository::getInstance()->removeGallery($id);

            wp_gdk_admin_list_galleries();

            break;
        default:
            wp_gdk_admin_list_galleries();
    }

    echo '</div>';
}

function wp_gdk_admin_update_order($gallery_id)
{
    if (!is_array($_POST['media_ids'])) {
        return;
    }

    WP_GDK_Repository::getInstance()->updateOrder($gallery_id, $_POST['media_ids']);
}

/**
 * Show gallery page
 *
 * @param int $id Gallery ID
 */
function wp_gdk_all_media($id)
{
    $repo = WP_GDK_Repository::getInstance();
    $gallery = $repo->getGallery($id);

    if (empty($gallery)) {
        wp_gdk_print_error(__('Requested gallery does not exists or removed.', 'wp-gdk'));
    }

    $items = $repo->getAllMedia($id);

    include plugin_dir_path(__FILE__) . 'src/inc/gallery_items.php';
}

/**
 * Add a media to provided gallery
 *
 * @param int $id Gallery ID
 */
function wp_gdk_add_medias($id)
{
    $wponce = filter_input(INPUT_POST, '_wpnonce');

    if (!wp_verify_nonce($wponce)) {
        return;
    }

    $mode = filter_input(INPUT_POST, 'wp_gdk_insert_mode');

    if ('multiple' === $mode) {
        $urlsRaw = filter_input(INPUT_POST, 'wp_gdk_multiple_media_input');
        $urls = preg_split("@\n@", $urlsRaw, null, PREG_SPLIT_NO_EMPTY);
        $fails = false;

        foreach ($urls as $url) {
            $url = trim($url);

            if (preg_match("/^[^\?]+\.(jpg|jpeg|gif|png)(?:\?|$)/", $url)) {
                WP_GDK_Repository::getInstance()->addMedia($id, $url);
            }

            $fails = true;
        }

        if ($fails) {
            wp_gdk_print_error(__('Some provided URLs are not valid.', 'wp-gdk'));
        }

        return;
    }

    $url = filter_input(INPUT_POST, 'wp_gdk_single_media_url');
    $link = filter_input(INPUT_POST, 'wp_gdk_resource_input');
    $title = filter_input(INPUT_POST, 'wp_gdk_title_input');

    if (!preg_match("/^[^\?]+\.(jpg|jpeg|gif|png)(?:\?|$)/", $url)) {
        wp_gdk_print_error('Not a valid image URL!');

        return;
    }

    if (!empty($link) && !wp_http_validate_url($link)) {
        wp_gdk_print_error(__('Resource must be a valid URL!', 'wp-gdk'));

        return;
    }

    WP_GDK_Repository::getInstance()->addMedia($id, $url, $link, $title);
}

/**
 * Insert new gallery
 */
function wp_gdk_admin_save_gallery()
{
    $wponce = filter_input(INPUT_POST, '_wpnonce');

    if (!wp_verify_nonce($wponce)) {
        return;
    }

    $id = filter_input(INPUT_GET, 'gallery_id');
    $name = filter_input(INPUT_POST, 'wp_gdk_gallery_name');
    $description = filter_input(INPUT_POST, 'wp_gdk_gallery_description');

    if (!empty($id)) {
        WP_GDK_Repository::getInstance()->updateGallery($id, $name, $description);
    } else {
        $id = WP_GDK_Repository::getInstance()->addGallery($name, $description);
    }

    wp_gdk_all_media($id);
}

/**
 * List add galleries
 */
function wp_gdk_admin_list_galleries()
{
    $wp_list_table = new WP_GDK_List_Table();
    $wp_list_table->prepare_items();

    echo sprintf('<h1 class="wp-heading-inline">%s</h1>', get_admin_page_title());
    echo sprintf(
        '<a href="%s" class="page-title-action">%s</a>',
        admin_url('admin.php?' . http_build_query([
            'page' => 'wp-gdk-admin-page',
            'action' => 'add_gallery'
        ])),
        __('Add')
    );
    echo '<hr class="wp-header-end">';

    $wp_list_table->display();
}

/**
 * Edit gallery properties
 *
 * @param int|null $gallery_id
 */
function wp_gdk_admin_edit_gallery($gallery_id = null)
{
    if ($gallery_id !== null) {
        $gallery = WP_GDK_Repository::getInstance()->getGallery($gallery_id);

        if (empty($gallery)) {
            wp_gdk_print_error(__('Requested gallery does not exist!', 'wp-gdk'));

            return;
        }
    }

    include plugin_dir_path(__FILE__) . 'src/inc/gallery_edit_form.php';
}

/**
 * Display WordPress styled error message
 *
 * @param string $message
 */
function wp_gdk_print_error($message)
{
    echo sprintf('<div class="error notice"><p>%s</p></div>', $message);
}

/**
 * @param $gallery_id
 *
 * @return string
 */
function wp_gdk_get_edit_media_url($gallery_id)
{
    return admin_url('admin.php?' . http_build_query([
        'page' => 'wp-gdk-admin-page',
        'action' => 'edit_items',
        'gallery_id' => $gallery_id
    ]));
}

/**
 * Init plugin on activation
 */
function wp_gdk_activation()
{
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    global $wpdb;

    $galleriesTable = $wpdb->prefix . WP_GDK_Repository::TABLE_GALLERIES;
    $mediaTable = $wpdb->prefix . WP_GDK_Repository::TABLE_MEDIA;

    dbDelta("
        CREATE TABLE `{$galleriesTable}` (
          `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
          `name` varchar(128) NOT NULL,
          `description` varchar(2048) NOT NULL,
          PRIMARY KEY (id)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
        
        CREATE TABLE `{$mediaTable}` (
          `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
          `gallery_id` int(10) UNSIGNED NOT NULL,
          `url` varchar(255) NOT NULL,
          `link` varchar(255) DEFAULT NULL,
          `title` varchar(255) DEFAULT NULL,
          `sort` int(3) UNSIGNED NOT NULL DEFAULT '0',
          PRIMARY KEY (id)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
    ");
}
