(function($) {
    $(document).ready(function() {
        $('#wp_gdk_insert_multiple_media').click(function() {
            $('#wp_gdk_single_media_form').hide();
            $('#wp_gdk_multiple_media_form').show();
        });

        $('#wp_gdk_insert_single_media').click(function() {
            $('#wp_gdk_multiple_media_form').hide();
            $('#wp_gdk_single_media_form').show();
        });

        $('#wp_gdk_select_single_media_btn').click(function(e) {
            e.preventDefault();

            let mediaUploader = wp.media.frames.file_frame = wp.media({
                multiple: false
            });

            mediaUploader.on('select', function () {
                let attachment = mediaUploader.state().get('selection').first().toJSON();

                $('#wp_gdk_single_media_input').val(attachment.url);
            });

            mediaUploader.open();
        });

        $('#wp_gdk_select_multiple_media_btn').click(function(e) {
            e.preventDefault();

            let mediaUploader = wp.media.frames.file_frame = wp.media({
                multiple: true
            });

            mediaUploader.on('select', function () {
                let attachments = mediaUploader.state().get('selection').toJSON();
                let urls = [];

                for (let i in attachments) {
                    urls.push(attachments[i].url);
                }

                $('#wp_gdk_multiple_media_input').val(urls.join("\n"));
            });

            mediaUploader.open();
        });

        $('a[data-action=remove]').click(function() {
            if (!confirm('Do you want to delete this item?')) {
                return false;
            }
        });

        $('.wp-gdk-gallery-items .the-list').sortable({
            handle: '.wp-gdk-drag-and-drop',
            itemSelector: '.wp-gdk-gallery-item',
            placeholder: 'wp-gdk-list-placeholder',
            update: function() {
                let mediaIds = [];

                $(this).find('[data-mediaid]').each(function() {
                    mediaIds.push($(this).data('mediaid'));
                });

                $.ajax({
                    type: "POST",
                    url: $('[data-sorturl]').data('sorturl'),
                    data: {
                        'media_ids': mediaIds
                    }
                });
            }
        });

        (new ClipboardJS('[data-clipboard-text]')).on('success', function(e) {
            let trigger = $(e.trigger);
            let previousText = trigger.html();

            trigger.html('Copied!');

            setTimeout(function() {
                trigger.html(previousText);
            }, 500)
        });
    });
})(jQuery);
